package command

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"os/exec"
)

type ExecutionSummary struct {
	StdOut  string
	StdErr  string
	Success bool
}

func Execute(name string, env map[string]string, args ...string) (*ExecutionSummary, error) {
	//log.Printf("Running \"%v%v\"", name, ConcatArgs(args))
	cmd := exec.Command(name, args...)
	for key, value := range env {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", key, value))
	}

	stdout := bytes.Buffer{}
	stderr := bytes.Buffer{}

	cmd.Stdout = bufio.NewWriter(&stdout)
	cmd.Stderr = bufio.NewWriter(&stderr)

	if err := cmd.Run(); err != nil {
		return nil, errors.New(fmt.Sprintf("Error running %v, see \"%v\", %v", name, Concat(AsString(&stdout), AsString(&stderr), err.Error())))
	}
	return &ExecutionSummary{StdOut: AsString(&stdout), StdErr: AsString(&stderr), Success: cmd.ProcessState.Success()}, nil
}

func AsString(reader *bytes.Buffer) string {
	return string(reader.Bytes())
}

func ConcatArgs(messages []string) string {
	var concat string
	for i := range messages {
		message := messages[i]
		concat = concat + " " + message
	}
	return concat
}

func Concat(messages ...string) string {
	var concat string
	for i := range messages {
		message := messages[i]
		concat = concat + message
	}
	return concat
}
