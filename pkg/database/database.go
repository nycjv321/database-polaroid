package database

type DatabaseManager interface {
	Dump(destination string) error
	Restore(source string) error
}

type Configuration struct {
	Username  string
	Password  string
	Databases []string
}
