package ui

import (
	"database-polaroid/pkg/database"
	"database-polaroid/pkg/database/mongo"
	"database-polaroid/pkg/database/postgres"
	"fmt"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"io/ioutil"
	"log"
	"path/filepath"
	"strconv"
	"strings"
)

type DatabaseSnapshotManagerUI struct {
	snapshots     *widgets.List
	tools         *widgets.Paragraph
	configuration *widgets.Paragraph
	logs          *widgets.Paragraph
	input         *widgets.Paragraph
	x             int
	y             int
	Databases     Databases
	SnapshotPath  string
}

func (d DatabaseSnapshotManagerUI) Database() database.DatabaseManager {
	switch CurrentDatabase {
	case PostgresLabel:
		return d.Databases.Postgres
	case MongoDbLabel:
		return d.Databases.Mongo
	default:
		panic("unsupported database")
	}

}

type Databases struct {
	Postgres postgres.DatabaseManager
	Mongo    mongo.DatabaseManager
}

func (d DatabaseSnapshotManagerUI) log(message string) {
	logs := d.logs
	existingText := logs.Text
	if len(existingText) == 0 {
		logs.Text = message
	} else {
		logs.Text = existingText + "\n" + message
	}
	ui.Render(logs)
}

func (p DatabaseSnapshotManagerUI) Dimensions() (int, int) {
	return p.x, p.y
}

func (p DatabaseSnapshotManagerUI) resetInput() {
	// add check to only redraw when needed
	input := p.input
	input.Title = "Input"
	input.Text = ""
	ui.Render(input)
}

func TurnOff() {
	ui.Close()
}

func CreateUi(snapshotPath string,  postgresConfig database.Configuration, mongoConfig database.Configuration) *DatabaseSnapshotManagerUI {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v", err)
	}

	maxX, maxY := ui.TerminalDimensions()

	snapshots := SnapshotsList(0, 0, 25, maxY)
	tools := ToolsWindow(25, 0, (maxX/2)+12, 5, maxX)
	configuration := GeneralConfiguration(maxX/2+13, 0, maxX, 5)
	inputArea := Input(25, 5, maxX, 8)
	logsArea := Logs(25, 8, maxX, maxY)

	ui.Render(configuration, inputArea, logsArea, tools, snapshots)

	postgresEnv := map[string]string{
		"PGPASSWORD": postgresConfig.Password,
		"PGUSER":     postgresConfig.Username,
	}

	a := postgres.DatabaseManager{Databases: postgresConfig.Databases, Env: postgresEnv}
	b := mongo.DatabaseManager{Configuration: mongoConfig}

	databases := Databases{Postgres: a, Mongo: b}
	polaroid := DatabaseSnapshotManagerUI{snapshots: snapshots, tools: tools,
		input: inputArea, configuration: configuration, logs: logsArea,
		x: maxX, y: maxY, Databases: databases, SnapshotPath: snapshotPath}
	return &polaroid
}

func (p DatabaseSnapshotManagerUI) redrawButtons(selection int) {
	p.resetInput()
	x, _ := p.Dimensions()
	if selection == 1 {
		p.tools.Text = CreateTextButtons(CreateSnapshotLabel, (x/2)-25, RestoreSnapshotLabel, CreateSnapshotLabel)
		p.configuration.Text = CreateTextButtons(ButtonLabel{Position: -1}, (x/2)-25, SwitchDatabaseLabel, ChangeSnapshotLabel)
	} else if selection == 0 {
		p.tools.Text = CreateTextButtons(RestoreSnapshotLabel, (x/2)-25, RestoreSnapshotLabel, CreateSnapshotLabel)
		p.configuration.Text = CreateTextButtons(ButtonLabel{Position: -1}, (x/2)-25, SwitchDatabaseLabel, ChangeSnapshotLabel)
	} else if selection == 2 {
		p.configuration.Text = CreateTextButtons(SwitchDatabaseLabel, (x/2)-25, SwitchDatabaseLabel, ChangeSnapshotLabel)
		p.tools.Text = CreateTextButtons(ButtonLabel{Position: -1}, (x/2)-25, RestoreSnapshotLabel, CreateSnapshotLabel)
	} else if selection == 3 {
		p.configuration.Text = CreateTextButtons(ChangeSnapshotLabel, (x/2)-25, SwitchDatabaseLabel, ChangeSnapshotLabel)
		p.tools.Text = CreateTextButtons(ButtonLabel{Position: -1}, (x/2)-25, RestoreSnapshotLabel, CreateSnapshotLabel)
	}

	ui.Render(p.tools, p.configuration)
}

func readUserInput(output *widgets.Paragraph, userInput chan string) string {
	var line string
	for {
		select {
		case input := <-userInput:
			switch input {
			case "<Backspace>":
				if len(line) == 0 {
					continue
				}
				line = line[0 : len(line)-1]
				output.Text = line
				ui.Render(output)
			case "<Enter>":
				return line
			default:
				line = line + input
				output.Text = line
				ui.Render(output)
			}
		}
	}
}

func ConfigureSnapshotFileName(paragraph *widgets.Paragraph, userInput chan string) string {
	paragraph.Title = "Configure Snapshot File Name (valid filename characters include: [a-zA-Z0-9-_.])"
	paragraph.Text = "<What do you want to name your snapshot?>"
	return readUserInput(paragraph, userInput)
}

func DisplayError(paragraph *widgets.Paragraph, err error) {
	paragraph.Title = "Ran into an error!"
	paragraph.Text = err.Error()
	ui.Render(paragraph)
}

func DisplayMessage(paragraph *widgets.Paragraph, message string) {
	paragraph.Title = "Message"
	paragraph.Text = message
	ui.Render(paragraph)
}

func ConfirmSnapshot(paragraph *widgets.Paragraph, input string, userInput chan string) bool {
	paragraph.Title = "Confirm Operation"
	paragraph.Text = "Save a snapshot of \"" + CurrentDatabase.Label + "\" to \"" + input + "\" ? (Y/n)"
	ui.Render(paragraph)
	return strings.ToLower(readUserInput(paragraph, userInput)) == "y"
}
func ConfirmRestore(paragraph *widgets.Paragraph, input string, userInput chan string) bool {
	paragraph.Title = "Confirm Operation"
	paragraph.Text = "Restore of \"" + input + "\" to \"" + CurrentDatabase.Label + "\" ? (Y/n)"
	ui.Render(paragraph)
	return strings.ToLower(readUserInput(paragraph, userInput)) == "y"
}

func (p DatabaseSnapshotManagerUI) Restore(selectedSnapshot string, userInput chan string) {
	if ConfirmRestore(p.input, selectedSnapshot, userInput) {
		if err := p.Database().Restore(p.SnapshotPath + "/" + strings.ToLower(CurrentDatabase.Label) + "/" + strings.Split(selectedSnapshot, " ")[1]); err != nil {
			p.log(fmt.Sprintf("Unable to restore %v", selectedSnapshot))
			DisplayError(p.input, err)
		} else {
			p.log(fmt.Sprintf("Successfully restored %v", selectedSnapshot))
			DisplayMessage(p.input, fmt.Sprintf("Successfully restored %v", selectedSnapshot))
		}
	} else {
		p.resetInput()
	}
}

func (p DatabaseSnapshotManagerUI) Dump(fileName string, userInput chan string) {
	if ConfirmSnapshot(p.input, fileName, userInput) {
		if err := p.Database().Dump(p.SnapshotPath + "/" + strings.ToLower(CurrentDatabase.Label) + "/" + fileName); err != nil {
			p.log(fmt.Sprintf("Unable to dump %v to %v", "recipes", fileName))
			DisplayError(p.input, err)
		} else {
			p.log(fmt.Sprintf("Successfully dumped %v to %v", "recipes", fileName))
			DisplayMessage(p.input, fmt.Sprintf("Successfully dumped %v to %v", "recipes", fileName))
		}

		p.snapshots.Rows = append(p.snapshots.Rows, "["+strconv.Itoa(len(p.snapshots.Rows))+"] "+fileName)
		ui.Render(p.snapshots)
	} else {
		p.resetInput()
	}
}
func (p DatabaseSnapshotManagerUI) handleActions(selection int, userInput chan string) {
	switch selection {
	case RestoreSnapshotLabel.Position:
		ActiveWindow = Window{Block: &p.input.Block}
		go func() {
			rows := p.snapshots.Rows
			if len(rows) > 0 {
				selectedSnapshot := rows[p.snapshots.SelectedRow]
				p.Restore(selectedSnapshot, userInput)
			}
			ActiveWindow = Window{}

		}()
	case CreateSnapshotLabel.Position:
		ActiveWindow = Window{Block: &p.input.Block}
		go func() {
			fileName := ConfigureSnapshotFileName(p.input, userInput)
			p.Dump(fileName, userInput)
			ActiveWindow = Window{}
		}()
	case 2 + SwitchDatabaseLabel.Position:
		if CurrentDatabase == MongoDbLabel {
			CurrentDatabase = PostgresLabel
		} else {
			CurrentDatabase = MongoDbLabel
		}
		p.snapshots.Title = " " + CurrentDatabase.Emoji + "Snapshots"
		p.tools.Title = CurrentDatabase.Label + " Tools (keyboard activated - <Enter>/←/→)"

		p.loadSnapshots()
		ui.Render(p.tools)
	case 2 + ChangeSnapshotLabel.Position:
		p.input.Title = "Configure Snapshot"
		p.input.Text = "<Where do you want to save your snapshots to?>"
	}
	ui.Render(p.input, p.tools)
}

func (p DatabaseSnapshotManagerUI) loadSnapshots() {
	snapshotPath := p.SnapshotPath

	if dir, err := ioutil.ReadDir(snapshotPath + "/" + strings.ToLower(CurrentDatabase.Label)); err == nil {
		p.snapshots.Rows = make([]string, 0, 0)
		for i := range dir {
			file := dir[i]
			fileName := filepath.Base(file.Name())
			label := strconv.Itoa(i)
			p.snapshots.Rows = append(p.snapshots.Rows, "["+label+"] "+fileName)
		}
		ui.Render(p.snapshots)
		DisplayMessage(p.input, "Loaded "+CurrentDatabase.String()+" snapshosts")

	} else {
		p.snapshots.Rows = make([]string, 0, 0)
		DisplayError(p.input, err)
	}
}

func (p DatabaseSnapshotManagerUI) Capture() {
	p.loadSnapshots()

	previousKey := ""
	uiEvents := ui.PollEvents()
	selection := 0
	userInput := make(chan string)
	for {
		e := <-uiEvents
		listHandler(p.snapshots, p.input, e)
		if !inputHandler(p.input, userInput, e) {
			switch e.ID {
			case "<Enter>":
				p.handleActions(selection, userInput)
			case "q", "<C-c>":
				return
			case "<Right>":
				if selection <= 2 {
					selection += 1
				} else {
					selection = 0
				}
				p.redrawButtons(selection)
			case "<Left>":
				if selection > 0 {
					selection -= 1
				} else {
					selection = 3
				}
				p.redrawButtons(selection)
				//default:
				//  print(e.ID)
			}
		}
		if previousKey == "g" {
			previousKey = ""
		} else {
			previousKey = e.ID
		}
	}
}
