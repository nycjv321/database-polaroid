package ui

import (
	"fmt"
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
	"strings"
)

var toolsText string

var RestoreSnapshotLabel = ButtonLabel{Position: 0, Label: "[restore snapshot (\"r\")]"}
var CreateSnapshotLabel = ButtonLabel{Position: 1, Label: "[create snapshot (\"c\")]"}
var SwitchDatabaseLabel = ButtonLabel{Position: 0, Label: "[switch database (\"s\")]"}

//var ChangeSnapshotLabel = ButtonLabel{Position: 1, Label: "[change snapshot (\"p\")]"}
var ChangeSnapshotLabel = ButtonLabel{Position: 1, Label: "[placeholder for now]"}
var PostgresLabel = FancyLabel{Label: "Postgres", Emoji: "🐘"}
var MongoDbLabel = FancyLabel{Label: "Mongo", Emoji: "🍃"}
var CurrentDatabase = PostgresLabel
var ActiveWindow Window

type ButtonLabel struct {
	Position int
	Label    string
}

type FancyLabel struct {
	Label string
	Emoji string
}

type Window struct {
	Block *ui.Block
}

func (f FancyLabel) String() string {
	return fmt.Sprintf("%s (%s)", f.Label, f.Emoji)
}

func init() {
	toolsText = CreateTextButtons(ButtonLabel{}, 0, ButtonLabel{})
}

func CreateTextButtons(activeButton ButtonLabel, panelWidth int, buttons ...ButtonLabel) string {
	if len(buttons) == 0 || panelWidth == 0 {
		return ""
	}
	totalLength := getTextLengths(buttons)

	whiteSpace := panelWidth - totalLength
	whiteSpaceBlockSize := strings.Repeat(" ", whiteSpace/(len(buttons)+1))

	var text = "\n" + whiteSpaceBlockSize

	for i := range buttons {
		if i == activeButton.Position {
			text = text + buttons[i].Label + "(fg:grey)"
		} else {
			text = text + buttons[i].Label
		}
		if i != len(buttons) {
			text = text + whiteSpaceBlockSize
		}
	}
	return text
}

func getTextLengths(texts []ButtonLabel) int {
	var totalLength int
	for i := range texts {
		textLength := len(texts[i].Label)
		totalLength += textLength
	}
	return totalLength
}

func Logs(x1, y1, x2, y2 int) *widgets.Paragraph {
	p2 := widgets.NewParagraph()
	p2.Title = "Logs"
	p2.Text = ""
	p2.SetRect(x1, y1, x2, y2)
	return p2
}

func Input(x1, y1, x2, y2 int) *widgets.Paragraph {
	p2 := widgets.NewParagraph()
	p2.Title = "Input"
	p2.Text = ""
	p2.SetRect(x1, y1, x2, y2)
	return p2
}

func GeneralConfiguration(x1, y1, x2, y2 int) *widgets.Paragraph {
	pa := widgets.NewParagraph()
	pa.Title = "General Configuration"
	pa.Text = "\n         [switch database (\"s\")]    [change snapshot path (\"p\")] "
	pa.SetRect(x1, y1, x2, y2)
	return pa
}

func ToolsWindow(x1, y1, x2, y2, maxX int) *widgets.Paragraph {
	p0 := widgets.NewParagraph()
	p0.Title = CurrentDatabase.String() + " Tools (keyboard activated - <Enter>/←/→)"

	buttons := CreateTextButtons(RestoreSnapshotLabel, (maxX/2)-25, RestoreSnapshotLabel, CreateSnapshotLabel)
	p0.Text = buttons
	p0.SetRect(x1, y1, x2, y2)
	return p0
}

func SnapshotsList(x1, y1, x2, maxY int) *widgets.List {
	l := widgets.NewList()
	l.Title = " " + PostgresLabel.Emoji + "Snapshots"
	l.Rows = []string{}
	l.TextStyle = ui.NewStyle(ui.ColorWhite)
	l.SelectedRowStyle = ui.NewStyle(ui.ColorGreen)
	l.WrapText = false
	l.SetRect(x1, y1, x2, maxY)
	return l
}
