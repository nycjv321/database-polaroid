package ui

import (
	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

func inputHandler(inputArea *widgets.Paragraph, userInput chan string, event ui.Event) bool {
	if ActiveWindow.Block == &inputArea.Block {
		eventId := event.ID
		if eventId == "<Enter>" || eventId == "<Backspace>" {
			select {
			case userInput <- eventId:
			default:
			}
		} else if includesAllowedInput(eventId) {
			select {
			case userInput <- eventId:
			default:
			}
		}
		return true
	} else {
		return false
	}
}

func listHandler(snapshotList *widgets.List, paragraph *widgets.Paragraph, event ui.Event) {
	if ActiveWindow.Block != &paragraph.Block {
		switch event.ID {
		case "j", "<Down>":
			snapshotList.ScrollDown()
		case "k", "<Up>":
			snapshotList.ScrollUp()
		case "<C-d>":
			snapshotList.ScrollHalfPageDown()
		case "<C-u>":
			snapshotList.ScrollHalfPageUp()
		case "<C-f>":
			snapshotList.ScrollPageDown()
		case "<C-b>":
			snapshotList.ScrollPageUp()
		case "<Home>":
			snapshotList.ScrollTop()
		case "G", "<End>":
			snapshotList.ScrollBottom()
		}

		ui.Render(snapshotList)
	}
}
