package ui

import (
	"strconv"
	"strings"
)

var alphabet = []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"}
var numbers = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
var specialCharacters = []string{"-", "_", "."}

func includeStringCaseInsensitive(array []string, input string) bool {
	for i := range array {
		if strings.ToLower(array[i]) == strings.ToLower(input) {
			return true
		}
	}
	return false
}

func includesNumber(array []int, input int) bool {
	for i := range array {
		if array[i] == input {
			return true
		}
	}
	return false
}

func asNumber(input string) (int, bool) {
	n, err := strconv.Atoi(input)
	return n, err == nil

}

func includesAllowedInput(character string) bool {
	number, isNumber := asNumber(character)
	return (includeStringCaseInsensitive(alphabet, character) || includeStringCaseInsensitive(specialCharacters, character)) || isNumber && includesNumber(numbers, number)
}
