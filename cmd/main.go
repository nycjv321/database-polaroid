package main

import (
  "database-polaroid/pkg/database"
  . "database-polaroid/pkg/ui"
  "flag"
  "fmt"
  "os"
  "os/user"
  "strings"
)

var postgresDatabases = flag.String("postgres-databases", "", "postgres databases to snapshot/restore")
var mongoDatabases = flag.String("mongo-databases", "", "mongo databases to snapshot/restore")
var mongoUser = flag.String("mongo-user", "", "mongo user to authenticate as")
var mongoPassword = flag.String("mongo-password", "", "mongo password to authenticate with")
var postgresUser = flag.String("postgres-user", "", "postgres user to authenticate as")
var postgresPassword = flag.String("postgres-password", "", "postgres password to authenticate with")
var snapshotPath = flag.String("snapshot-path", "", "path to store snapshots")

func getSnapshotPath() string {
  if len(*snapshotPath) == 0 {
    if current, err := user.Current(); err == nil {
      return fmt.Sprintf("%v/.database_polaroid", current.HomeDir)
    } else {
      panic(err)
    }
  } else {
    return *snapshotPath
  }

}

func parseArrayArg(arg string) []string {
  args := strings.Split(arg, ",")
  output := make([]string, len(args))
  for i := range args {
    output[i] = strings.TrimSpace(args[i])
  }
  return output
}

func initializeConfiguration() (postgresConfig database.Configuration, mongoConfig database.Configuration) {
  flag.Parse()

  mongoConfiguration := database.Configuration{
    Username:  *mongoUser,
    Password:  *mongoPassword,
    Databases: parseArrayArg(*mongoDatabases),
  }

  postgresConfiguration := database.Configuration{
    Username:  *postgresUser,
    Password:  *postgresPassword,
    Databases: parseArrayArg(*postgresDatabases),
  }

  return postgresConfiguration, mongoConfiguration
}

func createDirectories(dir ...string) {
  for i := range dir {
    if err := os.MkdirAll(dir[i], os.ModePerm); err != nil {
      panic(err)
    }
  }

}

func main() {
  postgresConfig, mongoConfig := initializeConfiguration()
  snapshotPath := getSnapshotPath()
  createDirectories(snapshotPath, snapshotPath + "/" + "postgres", snapshotPath + "/" +"mongo")
  databaseManager := CreateUi(snapshotPath, postgresConfig, mongoConfig)
  databaseManager.Capture()
  TurnOff()
}
