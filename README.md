# Database Polaroid

Create and manage database snapshots for Postgres and Mongo

![alt text](docs/preview.png "Preview Image")


## Building

    go build -o build ./...
    
## Creating a mongo db
    docker run --name mongo -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=some_user -e MONGO_INITDB_ROOT_PASSWORD=some_user_pass -d mongo:4.2.6
